/*------------------------------------------------------------------
 Binary Search Tree.cpp
 The Votuporanga Disaster

 Implementation of a string-based binary search tree.
 Each binary search tree represents all the survivors' names of the votuporanga disaster registered in the system.

 Created by Neemias Freitas on 11/2/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Binary Search Tree.h"

using namespace std;


// Manager Methods (constructors & destructors)
/*------------------------------------------------------------------
 The default binary search tree constructor.
 ------------------------------------------------------------------*/
BST::BST(void) {
	currentSection = 0;
	treeSize = 0;
	count = 0;
	root = nullptr;
}

/*------------------------------------------------------------------
 The default binary search tree destructor, deleting all tree nodes.
 ------------------------------------------------------------------*/
BST::~BST(void) {
	if (this->root) {
		while (this->treeSize != 1) {
			this->deleteNode(this->root);
			treeSize--;
		}
		delete this->root;
		this->root = nullptr;
	}
}


// Accessor Methods
/*------------------------------------------------------------------
 getCount returns the number of first names registered in the tree.
 ------------------------------------------------------------------*/
int BST::getCount(void) { return this->count; }

/*------------------------------------------------------------------
 getRoot returns the root tree node.
 ------------------------------------------------------------------*/
TreeNode* BST::getRoot(void) { return this->root; }

/*------------------------------------------------------------------
 searchFamilyName returns the tree node in which the given family name was found.
 If nothing is found, it returns a nullptr.
 Search is made by comparing the given family name with the current node's family name.
 When it matches, it returns that node. If there is no match, the node will be a nullptr.
 ------------------------------------------------------------------*/
TreeNode* BST::searchFamilyName(string familyName) {
	TreeNode* current = root;
	while (current) {
		if (familyName == current->getFamilyName())
			return current;
		else if (familyName < current->getFamilyName())
			current = current->getLeftChild();
		else if (familyName > current->getFamilyName())
			current = current->getRightChild();
	}
	return current;
}

/*------------------------------------------------------------------
 searchParent returns the parent tree node of the tree node in which the given family name is to be inserted.
 If the tree is empty (i.e. root = nullptr), this method is not called.
 Differently of searchFamilyName, it returns the last non-null node.
 ------------------------------------------------------------------*/
TreeNode* BST::searchParent(std::string familyName) {
	TreeNode* current = this->root;
	TreeNode* parent = this->root;
	while (current) {
		parent = current;
		if (familyName == current->getFamilyName())
			return parent;
		else if (familyName < current->getFamilyName())
			current = current->getLeftChild();
		else if (familyName > current->getFamilyName())
			current = current->getRightChild();
	}
	return parent;
}

/*------------------------------------------------------------------
 min returns the tree node which has the lowest string in the binary search tree starting from a given initial node.
 If the starting node is nullptr, it will return nullptr.
 ------------------------------------------------------------------*/
TreeNode* BST::min(TreeNode* start) {
	if (start) {
		while (start->getLeftChild())
			start = start->getLeftChild();
	}
	return start;
}

/*------------------------------------------------------------------
 successor returns the tree node which has the smallest string that is strictly greater than the string of a given initial node.
 ------------------------------------------------------------------*/
TreeNode* BST::successor(TreeNode* start) {
	if (start)
		return this->min(start->getRightChild());
	return start;
}

/*------------------------------------------------------------------
 print prints the full name of each survivor in the tree in alphabetical order, along with its section.
 Output pattern: "<familyName>, <firstName>"
 Before each list of first names, the section of the family is printed.
 ------------------------------------------------------------------*/
void BST::print(TreeNode* start) {
	if (start) {
		this->print(start->getLeftChild());
		set<string> firstNames = start->getFirstNames();
		string familyName = start->getFamilyName();
		cout << "Section " << start->getSection() << ":" << endl;
		for (set<string>::iterator it = firstNames.begin(); it != firstNames.end(); it++)
			cout << familyName << ", " << *it << endl;
		this->print(start->getRightChild());
	}
}


// Mutator Methods
/*------------------------------------------------------------------
 insert inserts a given full name into the binary search tree.
 ------------------------------------------------------------------*/
bool BST::insert(string name) {

	// Process string
	string firstName, lastName;
	istringstream iss(name);
	iss >> firstName;
	iss >> lastName;

	// Check section in which new name will be assigned
	int section = this->checkSection();

	// Particular case for root = nullptr
	if (!this->root)
		this->root = new TreeNode(lastName, firstName, nullptr, section);

	else {
		// Search for node in which name is to be inserted
		TreeNode* parent = this->searchParent(lastName);

		// If family name is already registered, set only first name
		if (lastName == parent->getFamilyName()) {

			// Calls set.insert(), i.e., if name is a duplicate, it will return false
			if (parent->addFirstName(firstName)) {
				this->count++;
				return true;
			}
			else
				return false;
		}

		// Otherwise, register new family and its first first name
		else if (lastName < parent->getFamilyName())
			parent->setLeftChild(new TreeNode(lastName, firstName, parent, section));
		else if (lastName > parent->getFamilyName())
			parent->setRightChild(new TreeNode(lastName, firstName, parent, section));
	}
	this->treeSize++;
	this->count++;
	return true;
}

/*------------------------------------------------------------------
 remove removes a given full name from the binary search tree.
 ------------------------------------------------------------------*/
bool BST::remove(string name) {

	// Process string
	string firstName, lastName;
	istringstream iss(name);
	iss >> firstName;
	iss >> lastName;

	// Search for node in which name is located
	TreeNode* familyNode = this->searchFamilyName(lastName);

	// If node is not null
	if (familyNode) {

		// Remove first name. If removeFirstName returns false, name was not found.
		if (familyNode->removeFirstName(firstName)) {
			this->count--;

			// Check if node has no first name left
			if (!familyNode->getNodeSize()) {  // empty node, so delete it
				this->treeSize--;
				this->freeSections.insert(familyNode->getSection());  // save new free section
				return this->deleteNode(familyNode);
			}
		}
		return true;
	}
	return false;
}

/*------------------------------------------------------------------
 deleteNode deletes a given node from the tree.

 Prerequisites: start != nullptr
 ------------------------------------------------------------------*/
bool BST::deleteNode(TreeNode* start) {

	// No child
	if (!start->getLeftChild() && !start->getRightChild()) {
		if (start->getParent()->getLeftChild() == start)
			start->getParent()->setLeftChild(nullptr);
		else
			start->getParent()->setRightChild(nullptr);
		delete start;
		return true;
	}

	// Two children
	else if (start->getLeftChild() && start->getRightChild()) {
		TreeNode* successorNode = this->successor(start);
		start->setFamilyName(successorNode->getFamilyName());
		start->setFirstNames(successorNode->getFirstNames());
		start->setSection(successorNode->getSection());
		this->deleteNode(successorNode);
	}

	// One child
	else {
		if (start->getLeftChild()) {
			start->setFamilyName(start->getLeftChild()->getFamilyName());
			start->setFirstNames(start->getLeftChild()->getFirstNames());
			this->deleteNode(start->getLeftChild());
		}
		else {
			start->setFamilyName(start->getRightChild()->getFamilyName());
			start->setFirstNames(start->getRightChild()->getFirstNames());
			this->deleteNode(start->getRightChild());
		}
	}
	return false;
}

/*------------------------------------------------------------------
 checkSection returns the next section number available.
 ------------------------------------------------------------------*/
int BST::checkSection(void) {
	int section;

	// Check if there is any free section which was freed by a previous remotion of a family
	if (!this->freeSections.empty()) {
		section = *freeSections.begin();
		freeSections.erase(freeSections.begin());  // Section taken, remove it from freeSections
	}

	// Otherwise, next section available is the value of currentSection
	else {
		section = this->currentSection;
		currentSection++;
	}
	return section;
}