/*------------------------------------------------------------------
 main.cpp
 The Votuporanga Disaster

 A test driver for The Votuporanga Disaster project.

 Created by Neemias Freitas on 11/2/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Binary Search Tree.h"
#include <fstream>

using namespace std;


int main(int argc, const char * argv[]) {

	// Declare variables & instantiation
	string name;
	int option = NULL;
	BST survivors;

	// Bulk insertion of 500 names
	ifstream names("names.txt");
	if (names.is_open()) {
		while (getline(names, name))
			survivors.insert(name);
	}

	// Main menu
	cout << "Welcome to Votuporanga Rescue System! (by Neemias Freitas)\
	\nThere are 500 survivors already registered in the system. To start...";

	while (option != 6) {

		// Get user's option
		cout << "\nChoose an option:\
		\n1) Search - search for survivors currently registered in the system (by family name)\
		\n2) Register - register a survivor in the system\
		\n3) Delete - delete a survivor from the system\
		\n4) Count - print the number of survivors currently registered in the system\
		\n5) Print - print <family-name, first-name> of all survivors currently registered in the system\
		\n6) Quit - quit the program\
		\nOption: ";
		cin >> option;
		cin.ignore (numeric_limits<streamsize>::max(), '\n');  // discard characters until newline is found

		// Access/execute chosen option
		switch (option) {
			default:
				cout << "\nChoose a valid option!";
				break;

			// SEARCH
			case 1: {
				cout << "\nEnter the family name to be searched: ";
				cin >> name;
				cin.ignore (numeric_limits<streamsize>::max(), '\n');  // discard characters until newline is found
				if (!survivors.searchFamilyName(name)) {
					cout << "\nNo survivor with the given family name was found. :/\n";
					break;
				}
				TreeNode* familyNode = survivors.searchFamilyName(name);	// find family node
				set<string> firstNames = familyNode->getFirstNames();		// get its first names
				int familySection = familyNode->getSection();				// get family section
				cout << firstNames.size() << " family member(s) found in section " << familySection << ":\n";
				copy(firstNames.begin(), firstNames.end(), ostream_iterator<string>(cout, "\n"));
				firstNames.clear();  // make sure it's empty to next search
				break;
			}

			// REGISTER
			case 2:
				cout << "\nEnter the full name of the new survivor: ";
				getline(cin, name);
				if (!survivors.insert(name)) {
					cout << "This survivor is already registered in the system!\n";
					break;
				}
				cout << "New survivor successfully registered! :D\n";
				break;

			// DELETE
			case 3:
				cout << "\nEnter the full name of the survivor to be deleted: ";
				getline(cin, name);
				if (!survivors.remove(name)) {
					cout << "This survivor is not registered in the system! :/\n";
					break;
				}
				cout << "Survivor successfully deleted from the system! :D\n";
				break;

			// COUNT
			case 4:
				if (!survivors.getCount()) { cout << "\nThere is no survivor registered in the system! :/\n"; }
				else { cout << "\nNumber of survivors registered: " << survivors.getCount() << endl; }
				break;

			// PRINT
			case 5:
				if (!survivors.getCount()) {
					cout << "\nThere is no survivor registered in the system! :/\n";
					break;
				}
				cout << "\nCurrent survivors in the system by family name:\n";
				survivors.print(survivors.getRoot());
				break;

			// QUIT
			case 6:
				survivors.~BST();
				break;
		};
	}
	return 0;
}