/*------------------------------------------------------------------
 Tree Node.cpp
 The Votuporanga Disaster

 Implementation of the tree node as a class.

 Created by Neemias Freitas on 11/5/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Tree Node.h"

using namespace std;


// Manager Methods (constructors & destructors)
/*------------------------------------------------------------------
 An optimized constructor for the tree node.
 ------------------------------------------------------------------*/
TreeNode::TreeNode(string familyName, string firstName, TreeNode* parent, int section) {
	this->familyName = familyName;
	this->section = section;
	this->parent = parent;
	this->leftChild = nullptr;
	this->rightChild = nullptr;
	this->addFirstName(firstName);
}


// Accessor Methods
/*------------------------------------------------------------------
 getFamilyName returns the family name whose object represents.
 ------------------------------------------------------------------*/
string TreeNode::getFamilyName(void) { return this->familyName; }

/*------------------------------------------------------------------
 getFirstNames returns the first name of each family member belonging to the same family name.
 The structure that contains the first names is a set.
 ------------------------------------------------------------------*/
set<string> TreeNode::getFirstNames(void) { return this->firstNames; }

/*------------------------------------------------------------------
 getSection returns the section whose family is.
 ------------------------------------------------------------------*/
int TreeNode::getSection(void) { return this->section; }

/*------------------------------------------------------------------
 getParent returns its parent node.
 ------------------------------------------------------------------*/
TreeNode* TreeNode::getParent(void) { return this->parent; }

/*------------------------------------------------------------------
 getLeftChild returns its left child node.
 ------------------------------------------------------------------*/
TreeNode* TreeNode::getLeftChild(void) { return this->leftChild; }

/*------------------------------------------------------------------
 getRightChild returns its right child node.
 ------------------------------------------------------------------*/
TreeNode* TreeNode::getRightChild(void) { return this->rightChild; }

/*------------------------------------------------------------------
 getNodeSize returns the current size of the family whose object represents.
 The size is given by the size of the family names set.
 ------------------------------------------------------------------*/
int TreeNode::getNodeSize(void) { return int(this->firstNames.size()); }


// Mutator Methods
/*------------------------------------------------------------------
 setFamilyName sets the family name whose object represents.
 ------------------------------------------------------------------*/
void TreeNode::setFamilyName(string familyName) { this->familyName = familyName; }

/*------------------------------------------------------------------
 setFirstNames sets the first name of each family member belonging to the same family name.
 The structure that contains the first names is a set.
 ------------------------------------------------------------------*/
void TreeNode::setFirstNames(set<string> firstNames) { this->firstNames = firstNames; }

/*------------------------------------------------------------------
 setSection sets the section whose family is.
 ------------------------------------------------------------------*/
void TreeNode::setSection(int section) { this->section = section; }

/*------------------------------------------------------------------
 setParent sets its parent node.
 ------------------------------------------------------------------*/
void TreeNode::setParent(TreeNode* parent) { this->parent = parent; }

/*------------------------------------------------------------------
 setLeftChild sets its left child node.
 ------------------------------------------------------------------*/
void TreeNode::setLeftChild(TreeNode* leftChild) { this->leftChild = leftChild; }

/*------------------------------------------------------------------
 setRightChild sets its right child node.
 ------------------------------------------------------------------*/
void TreeNode::setRightChild(TreeNode* rightChild) { this->rightChild = rightChild; }

/*------------------------------------------------------------------
 addFirstName adds a family member's first name to the set of first names.
 ------------------------------------------------------------------*/
bool TreeNode::addFirstName(string firstName) { return this->firstNames.insert(firstName).second; }

/*------------------------------------------------------------------
 removeFirstName removes a family member's first name from the set of first names.
 ------------------------------------------------------------------*/
bool TreeNode::removeFirstName(string firstName) { return this->firstNames.erase(firstName); }