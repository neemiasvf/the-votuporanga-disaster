/*------------------------------------------------------------------
 Tree Node.h
 The Votuporanga Disaster

 Definition of a tree node as a class.
 Each tree node represents only a family name (as a string) and its members (as a set of its first names).
 It also contains the section which the family is located and pointers to its parent, left and right child.

 Created by Neemias Freitas on 11/5/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#ifndef __The_Votuporanga_Disaster__Tree_Node__
#define __The_Votuporanga_Disaster__Tree_Node__
#include <string>
#include <set>


class TreeNode {

public:

	// Manager Methods (constructors & destructors)
	/*------------------------------------------------------------------
	 An optimized constructor for the tree node.
	 ------------------------------------------------------------------*/
	TreeNode(std::string familyName, std::string firstName, TreeNode* parent, int section);

	// Accessor Methods
	/*------------------------------------------------------------------
	 getFamilyName returns the family name whose object represents.
	 ------------------------------------------------------------------*/
	std::string getFamilyName(void);

	/*------------------------------------------------------------------
	 getFirstNames returns the first name of each family member belonging to the same family name.
	 The structure that contains the first names is a set.
	 ------------------------------------------------------------------*/
	std::set<std::string> getFirstNames(void);

	/*------------------------------------------------------------------
	 getSection returns the section whose family is.
	 ------------------------------------------------------------------*/
	int getSection(void);

	/*------------------------------------------------------------------
	 getParent returns its parent node.
	 ------------------------------------------------------------------*/
	TreeNode* getParent(void);

	/*------------------------------------------------------------------
	 getLeftChild returns its left child node.
	 ------------------------------------------------------------------*/
	TreeNode* getLeftChild(void);

	/*------------------------------------------------------------------
	 getRightChild returns its right child node.
	 ------------------------------------------------------------------*/
	TreeNode* getRightChild(void);

	/*------------------------------------------------------------------
	 getNodeSize returns the current size of the family whose object represents.
	 The size is given by the size of the family names set.
	 ------------------------------------------------------------------*/
	int getNodeSize(void);


	// Mutator Methods
	/*------------------------------------------------------------------
	 setFamilyName sets the family name whose object represents.
	 ------------------------------------------------------------------*/
	void setFamilyName(std::string familyName);

	/*------------------------------------------------------------------
	 setFirstNames sets the first name of each family member belonging to the same family name.
	 The structure that contains the first names is a set.
	 ------------------------------------------------------------------*/
	void setFirstNames(std::set<std::string> firstNames);

	/*------------------------------------------------------------------
	 setSection sets the section whose family is.
	 ------------------------------------------------------------------*/
	void setSection(int section);

	/*------------------------------------------------------------------
	 setParent sets its parent node.
	 ------------------------------------------------------------------*/
	void setParent(TreeNode* parent);

	/*------------------------------------------------------------------
	 setLeftChild sets its left child node.
	 ------------------------------------------------------------------*/
	void setLeftChild(TreeNode* leftChild);

	/*------------------------------------------------------------------
	 setRightChild sets its right child node.
	 ------------------------------------------------------------------*/
	void setRightChild(TreeNode* rightChild);

	/*------------------------------------------------------------------
	 addFirstName adds a family member's first name to the set of first names.
	 ------------------------------------------------------------------*/
	bool addFirstName(std::string firstName);

	/*------------------------------------------------------------------
	 removeFirstName removes a family member's first name from the set of first names.
	 ------------------------------------------------------------------*/
	bool removeFirstName(std::string firstName);


private:

	// Object instance data
	std::string familyName;
	std::set<std::string> firstNames;	// a set storing the first names for that family name
	int section;						// the section whose family is
	TreeNode* parent;
	TreeNode* leftChild;
	TreeNode* rightChild;
	
};

#endif /* defined(__The_Votuporanga_Disaster__Tree_Node__) */