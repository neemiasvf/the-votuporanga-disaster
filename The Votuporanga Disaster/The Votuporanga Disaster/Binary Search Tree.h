/*------------------------------------------------------------------
 Binary Search Tree.h
 The Votuporanga Disaster

 Definition of a string-based binary search tree.
 Each binary search tree represents all the survivors' names of the votuporanga disaster registered in the system.

 Created by Neemias Freitas on 11/2/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#ifndef __The_Votuporanga_Disaster__Binary_Search_Tree__
#define __The_Votuporanga_Disaster__Binary_Search_Tree__
#include <iostream>
#include <sstream>
#include "Tree Node.h"


class BST {

public:

	// Manager Methods (constructors & destructors)
	/*------------------------------------------------------------------
	 The default binary search tree constructor.
	 ------------------------------------------------------------------*/
	BST(void);

	/*------------------------------------------------------------------
	 The default binary search tree destructor, deleting all tree nodes.
	 ------------------------------------------------------------------*/
	~BST(void);


	// Accessor Methods
	/*------------------------------------------------------------------
	 getCount returns the number of first names registered in the tree.
	 ------------------------------------------------------------------*/
	int getCount(void);

	/*------------------------------------------------------------------
	 getRoot returns the root tree node.
	 ------------------------------------------------------------------*/
	TreeNode* getRoot(void);

	/*------------------------------------------------------------------
	 searchFamilyName returns the tree node in which the given family name was found.
	 If nothing is found, it returns a nullptr.
	 ------------------------------------------------------------------*/
	TreeNode* searchFamilyName(std::string familyName);

	/*------------------------------------------------------------------
	 print prints the full name of each survivor in the tree in alphabetical order, along with its section.
	 Output pattern: "<familyName>, <firstName>"
	 ------------------------------------------------------------------*/
	void print(TreeNode* start);


	// Mutator Methods
	/*------------------------------------------------------------------
	 insert inserts a given full name into the binary search tree.
	 ------------------------------------------------------------------*/
	bool insert(std::string name);
	/*------------------------------------------------------------------
	 remove removes a given full name from the binary search tree.
	 ------------------------------------------------------------------*/
	bool remove(std::string name);


private:

	// Accessor Methods
	/*------------------------------------------------------------------
	 searchParent returns the parent tree node of the tree node in which the given family name is to be inserted.
	 If the tree is empty (i.e. root = nullptr), this method is not called.
	 ------------------------------------------------------------------*/
	TreeNode* searchParent(std::string familyName);

	/*------------------------------------------------------------------
	 min returns the tree node which has the lowest string in the binary search tree starting from a given initial node.
	 If the starting node is nullptr, it will return nullptr.
	 ------------------------------------------------------------------*/
	TreeNode* min(TreeNode* start);

	/*------------------------------------------------------------------
	 successor returns the tree node which has the smallest string that is strictly greater than the string of a given initial node.
	 ------------------------------------------------------------------*/
	TreeNode* successor(TreeNode* start);


	// Mutator Methods
	/*------------------------------------------------------------------
	 deleteNode deletes a given node from the tree.

	 Prerequisites: start != nullptr
	 ------------------------------------------------------------------*/
	bool deleteNode(TreeNode* start);

	/*------------------------------------------------------------------
	 checkSection returns the next section number available.
	 ------------------------------------------------------------------*/
	int checkSection(void);


	// Object instance data
	std::set<int> freeSections;		// Next free section
	int currentSection;				// Greatest free section
	int treeSize;					// Number of nodes in the tree
	int count;						// Number of first names currently registered in the tree
	TreeNode* root;

};

#endif /* defined(__The_Votuporanga_Disaster__Binary_Search_Tree__) */